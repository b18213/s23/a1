function Pokemon(name, level){

	// Properties
	this.name = name;
	this.lvl = level;
	this.health = 2 * level;
	this.atk = level*.50;

	// Methods

	this.tackle = function(target){
		console.log(`${this.name} wants to attack!`)
		if(this.health <= 10){
			console.log("Pokemon can no longer battle")
		}else if(target.health <=10){
			console.log("Enemy already fainted")
		}else{
		console.log(`${this.name} tackled ${target.name}`)
		target.health = target.health - this.atk;
			if (target.health <= 10){
				target.health = 10;
			};
			console.log(`${target.name}'s health is now reduced to ${target.health}`);
		if (target.health <= 10) {
			console.log(`${target.name} fainted`)		
			};
		}			
			
	};
		
};

let Bulbasaur = new Pokemon("Bulbasaur", 50);
let Rattata = new Pokemon ("Rattata", 40);

// console.log(Bulbasaur);
// console.log(Rattata);

Bulbasaur.tackle(Rattata);
Rattata.tackle(Bulbasaur);
Bulbasaur.tackle(Rattata);
Bulbasaur.tackle(Rattata);
Rattata.tackle(Bulbasaur);








